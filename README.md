# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

git clone https://mkey97@bitbucket.org/mkey97/stroboskop.git

Naloga 6.2.3:

https://bitbucket.org/mkey97/stroboskop/commits/5676bf1eddf7ab7590546ce236dcad00635fbe4e

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:

https://bitbucket.org/mkey97/stroboskop/commits/d7959357e04771c7a776b25315520547ced58091

Naloga 6.3.2:

https://bitbucket.org/mkey97/stroboskop/commits/f54da38c2793366599e018931bbfdfdd1385ce6e

Naloga 6.3.3:

https://bitbucket.org/mkey97/stroboskop/commits/53349722ab7c6ec59055629d869dc641afaec7be

Naloga 6.3.4:

https://bitbucket.org/mkey97/stroboskop/commits/ddc794d2a0edf22b3ec01749be82ab7aae0ec415

Naloga 6.3.5:

git checkout master; 
git merge izgled;
git push origin master;

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:

https://bitbucket.org/mkey97/stroboskop/commits/c59dd943bc36bb73194fea951e2e64f822d732a5

Naloga 6.4.2:

https://bitbucket.org/mkey97/stroboskop/commits/21c62a8ca9e6af81498e5160f4cacf42b35935ec

Naloga 6.4.3:

https://bitbucket.org/mkey97/stroboskop/commits/9f53e6df0fd80cd929b58ffcc12a811bcf21f88e

Naloga 6.4.4:

https://bitbucket.org/mkey97/stroboskop/commits/11a7b9601028769eab74119b0f7cb44cb1ecb040